package Unitofwork;

import domains.Entity;

  public interface ImUnitOfWorkRepository {


	 public void persistAdd(Entity entity);
	 public void persistUpdate(Entity entity);
	 public void persistDelete(Entity entity);


}
