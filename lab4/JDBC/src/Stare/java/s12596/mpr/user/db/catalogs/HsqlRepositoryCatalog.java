package s12596.mpr.user.db.catalogs;

import java.sql.Connection;

import s12596.mpr.user.db.AdresRepository;
import s12596.mpr.user.db.PermissionRepository;
import s12596.mpr.user.db.PersonRepository;
import s12596.mpr.user.db.RepositoryCatalog;
import s12596.mpr.user.db.RoleRepository;
import s12596.mpr.user.db.UserRepository;
import s12596.mpr.user.db.repos.HsqlAddressRepository;
import s12596.mpr.user.db.repos.HsqlPermissionRepository;
import s12596.mpr.user.db.repos.HsqlPersonRepository;
import s12596.mpr.user.db.HsqlRoleRepository;
import s12596.mpr.user.db.HsqlUserRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}

	@Override
	public AdresRepository addresses() {
		return new HsqlAddressRepository(connection);
	}

	@Override
	public RoleRepository roles() {
		return new HsqlRoleRepository(connection);
	}

	@Override
	public PermissionRepository permissions() {
		return new HsqlPermissionRepository(connection);
	}

	@Override
	public UserRepository users() {
		return new HsqlUserRepository(connection);
	}

}