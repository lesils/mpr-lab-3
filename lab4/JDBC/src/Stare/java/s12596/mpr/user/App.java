﻿package s12596.mpr.user;

import java.util.List;

import s12596.mpr.user.db.AdresDbManager;
import s12596.mpr.user.db.PermissionDbManager;
import s12596.mpr.user.db.PersonDbManager;
import s12596.mpr.user.db.RoleDbManager;
import s12596.mpr.user.db.UserDbManager;
import s12596.mpr.user.module.Adres;
import s12596.mpr.user.module.Person;
import s12596.mpr.user.module.Role;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        PersonDbManager mgr = new PersonDbManager();
        AdresDbManager adm = new AdresDbManager();
        PermissionDbManager pdm = new PermissionDbManager();
        RoleDbManager rdm = new RoleDbManager();
        UserDbManager udm = new UserDbManager();
        
        
        List<Person> allPersons = mgr.getAll();
        
        for(Person p : allPersons){
        	System.out.println(p.getId()+" "+p.getName()+" "+p.getSurname());
        }
        mgr.deleteById(3);
        
        Person toUpdate = new Person();
        
        toUpdate.setName("Maciej");
        toUpdate.setSurname("konkol");
        toUpdate.setId(2);
        
        mgr.update(toUpdate);
    
List<Adres> allAddresses = adm.getAll();
        
        for(Adres p : allAddresses){
        	System.out.println(p.getId()+" "+p.getCity()+" "+p.getCountry()+" "+p.getStreet());
        }
        adm.deleteById(3);
        
        Adres Update = new Adres();
        
        Update.setCity("Gdynia");
        Update.setCountry("Poland");
        Update.setStreet("ulica");
        Update.setId(2);
        
        adm.update(Update);
        
        
List<Role> allRoles = rdm.getAll();
        
        for(Role r : allRoles){
        	System.out.println(r.getId()+" "+r.getId()+" "+r.getIdPermission()+" "+r.getName());
        }
        rdm.deleteById(3);
        
        Role Updates = new Role();
        
        Updates.setIdPermission(2);
        Updates.setIdUser(1);
        Updates.setName("maciejowice");
        Updates.setId(2);
        
        rdm.update(Updates);
    }
}
