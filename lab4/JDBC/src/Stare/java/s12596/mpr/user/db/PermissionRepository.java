package s12596.mpr.user.db;

import java.util.*;

import s12596.mpr.user.module.Permission;

public interface PermissionRepository extends Repository<Permission> {
	
	public List<Permission> withName(String name, PagingInfo page);
	
	
}