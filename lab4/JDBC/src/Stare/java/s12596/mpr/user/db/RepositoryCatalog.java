package s12596.mpr.user.db

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public AddressRepository addresses();
	public RoleRepository roles();
	public PermissionRepository permissions();
	public UserRepository users();
}