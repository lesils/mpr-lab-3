package s12596.mpr.user.db

/**
 * Created by Maciek on 2015-11-12.
 */
import java.util.List;

public interface Repository<TEntity> {

    public TEntity withId(int id);
    public List<TEntity> allOnPage(PagingInfo page);
    public void add(TEntity entity);
    public void modify(TEntity entity);
    public void remove(TEntity entity);
}