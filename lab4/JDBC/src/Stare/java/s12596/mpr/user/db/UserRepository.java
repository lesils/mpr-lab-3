package s12596.mpr.user.db;

import java.util.List;

import s12596.mpr.user.module.user;

public interface UserRepository extends Repository<User> {
	
	public List<User> withUsername(String username, PagingInfo page);

}