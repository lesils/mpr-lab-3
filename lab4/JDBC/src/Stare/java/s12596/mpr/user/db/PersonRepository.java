package s12596.mpr.user.db;

import java.util.*;

import s12596.mpr.user.module.Person;

public interface PersonRepository extends Repository<Person> {
	
	public List<Person> withSurname(String surname, PagingInfo page);
	public List<Person> withName(String name, PagingInfo page);
}