import java.sql.*;
import java.util.List;

import domain.*;
import repositories.*;
import repositories.impl.RepositoryCatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;


public class Main {

	public static void main(String[] args) {

		String url="jdbc:hsqldb:hsql://localhost/workdb";

		UserRoles userRoles1 = new UserRoles();
		userRoles1.setName("Administrator");
		userRoles1.setRoleId(rP1.getRoleId());
		userRoles1.setUserId(1);

		UserRoles userRoles2 = new UserRoles();
		userRoles2.setName("User l1");
		userRoles2.setRoleId(rP2.getRoleId());
		userRoles2.setUserId(2);

		UserRoles userRoles3 = new UserRoles();
		userRoles3.setName("User l2 user");
		userRoles3.setRoleId(rP3.getRoleId());
		userRoles3.setUserId(3);


		RolesPermissions rP1 = new RolesPermissions();
		rP1.setName(enumR1.getValue());
		rP1.setRoleId(enumR1.getIntKey());
		rP1.setPermissionId(enumP1.getIntKey());


			EnumerationValue enumR1 = new EnumerationValue();
		enumR1.setIntKey(1);
		enumR1.setStringKey("ADMROLE");
		enumR1.setValue("Admin");
		enumR1.setEnumerationName("ROLE");

		EnumerationValue enumR2 = new EnumerationValue();
		enumR2.setIntKey(2);
		enumR2.setStringKey("GUSROLE");
		enumR2.setValue("Global user ");
		enumR2.setEnumerationName("ROLE");

		EnumerationValue enumR3 = new EnumerationValue();
		enumR3.setIntKey(3);
		enumR3.setStringKey("LUSROLE");
		enumR3.setValue("Local user");
		enumR3.setEnumerationName("ROLE");


		EnumerationValue enumP1 = new EnumerationValue();
		enumP1.setIntKey(1);
		enumP1.setStringKey("FPERM");
		enumP1.setValue("Full privileges");
		enumP1.setEnumerationName("PERMISSION");

		EnumerationValue enumP2 = new EnumerationValue();
		enumP2.setIntKey(2);
		enumP2.setStringKey("GPERM");
		enumP2.setValue("Global privileges ");
		enumP2.setEnumerationName("PERMISSION");

		EnumerationValue enumP3 = new EnumerationValue();
		enumP3.setIntKey(3);
		enumP3.setStringKey("LPERM");
		enumP3.setValue("Local privileges");
		enumP3.setEnumerationName("PERMISSION");

		User u1 = new User();
		u1.setLogin("KonkolMaciej");
		u1.setPassword("Mpr2015");
		
		
		try {
			
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			
			IRepositoryCatalog catalog = new RepositoryCatalog(connection, uow);
			
			catalog.getEnumerationValue().save(enumR1);
			catalog.getEnumerationValue().save(enumP1);

			
			catalog.getRolesPermissions().save(rP1);

			
			catalog.getUsers().save(u1);

			catalog.getUserRoles().save(userRoles1);
					catalog.commit();
			
			
			
			
			List <EnumerationValue> tableEnums = catalog.getEnumerationValue().getAll();
			for(EnumerationValue tableEnum: tableEnums)
				System.out.println(tableEnum.getId()+" "+tableEnum.getIntKey()+" "
						+tableEnum.getStringKey()+" "+tableEnum.getValue()+" "
						+tableEnum.getEnumerationName());

			
			List<RolesPermissions> rPsFromDb= catalog.getRolesPermissions().getAll();
			
			for(RolesPermissions rPFromDb: rPsFromDb)
				System.out.println(rPFromDb.getId()+" "+rPFromDb.getName()+""
						+ " "+rPFromDb.getRoleId()+ " "+rPFromDb.getPermissionId());

			
			
			List<User> usersFromDb= catalog.getUsers().getAll();
			
			for(User userFromDb: usersFromDb)
				System.out.println(userFromDb.getId()+" "+userFromDb.getLogin()+""
						+ " "+userFromDb.getPassword());
			

			
			List<UserRoles> uRsFromDb= catalog.getUserRoles().getAll();
			
			for(UserRoles uRFromDb: uRsFromDb)
				System.out.println(uRFromDb.getId()+" "+uRFromDb.getName()+""
						+ " "+uRFromDb.getUserId()+ " "+uRFromDb.getRoleId());


			
			List<User> uRsFromDbwithroleName= catalog.getUsers().withRole("User l1");
			
			for(User uRFromDb: uRsFromDbwithroleName)
				System.out.println(uRFromDb.getId()+" "+uRFromDb.getLogin());


			
			List<User> uRsFromDbwithroleId= catalog.getUsers().withRole(1);
			
			for(User uRFromDb: uRsFromDbwithroleId)
				System.out.println(uRFromDb.getId()+" "+uRFromDb.getLogin());
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
