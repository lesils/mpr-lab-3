package domains;

public enum EntityState {
	New, Changed, Unchanged, Deleted
}
